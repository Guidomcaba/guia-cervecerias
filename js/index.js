$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    
    $('.carousel').carousel({
        interval:2000
    });
    $('#compra').on('show.bs.modal', function(e) {
        console.log('El modal se esta mostrando')
        
        $('#btn-compra').removeClass('btn-outline-primary');
        $('#btn-compra').addClass('btn-dark');
        $('#btn-compra').prop('disabled',true);
        $('#btn-compra-scd').removeClass('btn-outline-primary');
        $('#btn-compra-scd').addClass('btn-dark');
        $('#btn-compra-scd').prop('disabled',true);
    });
    $('#compra').on('shown.bs.modal', function(e) {
        console.log('El modal se mostró')
    });
    $('#compra').on('hide.bs.modal', function(e) {
        console.log('El modal se esta ocultando')
    });
    $('#compra').on('hidden.bs.modal', function(e) {
        console.log('El modal se ocultó')
        $('#btn-compra').removeClass('btn-dark');
        $('#btn-compra').addClass('btn-outline-primary');
        $('#btn-compra').prop('disabled',false);
        $('#btn-compra-scd').removeClass('btn-dark');
        $('#btn-compra-scd').addClass('btn-outline-primary');
        $('#btn-compra-scd').prop('disabled',false);
    });
});
